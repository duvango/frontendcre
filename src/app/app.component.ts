import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'crefront';
  cardText = `Tabla de todas las tarjetas que se encuentran registradas en la base de datos del sistema de prueba`;
  transactionText = `Tabla de todas las transacciones que se encuentran registradas en la base de datos del sistema de prueba`;

}

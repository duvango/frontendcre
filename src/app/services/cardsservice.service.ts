import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Card } from '../interfaces/CardInterface';
import { Transaction } from '../interfaces/TransactionInterface';


@Injectable({
  providedIn: 'root'
})
export class CardsserviceService {
  url= "http://localhost:8080/api/v1/"  
  constructor(private http: HttpClient) { }

  getAllCards(){
      return this.http.get<Card[]>(this.url+"card")

  }
  getAllTransactions(){
    return this.http.get<Transaction[]>(this.url+"transaction")
  }
  
}

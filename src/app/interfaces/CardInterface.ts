export interface Card {
    pan: string;
    cardHolder: string;
    personalIdentification :number;
    phone : number;
    status : string;
  }
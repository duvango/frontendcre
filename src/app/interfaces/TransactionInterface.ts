export interface Transaction {
    id: number;
    identificationSystem: string;
    referenceNumber: string;
    amount: number;
    address: string;
    status: string;
  }
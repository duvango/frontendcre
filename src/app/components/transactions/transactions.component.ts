import { Component, OnInit } from '@angular/core';
import { Transaction } from 'src/app/interfaces/TransactionInterface';
import { CardsserviceService } from 'src/app/services/cardsservice.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  constructor(private service:CardsserviceService) { }
  
  displayedColumns: string[] = ['id', 'identificationSystem', 'referenceNumber', 'amount', 'address', 'status'];
  dataSource: Transaction[];

  
  ngOnInit(): void {
    this.service.getAllTransactions().subscribe((data:Transaction[])=>{
      this.dataSource=data;
    });
  }

}


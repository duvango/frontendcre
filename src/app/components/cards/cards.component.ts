import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/interfaces/CardInterface';
import { CardsserviceService } from 'src/app/services/cardsservice.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  displayedColumns: string[] = ['pan', 'cardHolder', 'personalIdentification', 'phone', 'status'];
  dataSource : Card[];

  constructor(private service:CardsserviceService) { }

  ngOnInit(): void {
    this.service.getAllCards().subscribe((data:Card[])=>{
      this.dataSource=data;
    });
  }

}

